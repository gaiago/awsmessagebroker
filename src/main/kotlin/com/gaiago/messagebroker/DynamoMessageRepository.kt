package com.gaiago.messagebroker

import com.amazonaws.regions.Regions.EU_WEST_1
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item

class DynamoMessageRepository(tableName: String, dynamoDb: DynamoDB) : MessageRepository {
    private val table = dynamoDb.getTable(tableName)

    override fun alreadyNotified(message: Message): Boolean {
        val item = table.getItem("type", message.event, "id", message.value.id)
        return item != null
    }

    override fun save(message: Message) {
        table.putItem(Item().with("type", message.event).with("id", message.value.id))
    }

    companion object {
        fun defaultDynamoDb() = DynamoDB(AmazonDynamoDBClientBuilder.standard().withRegion(EU_WEST_1).build())
    }
}