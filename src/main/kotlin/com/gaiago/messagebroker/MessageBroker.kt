package com.gaiago.messagebroker

interface MessageBroker {
    fun publish(messageEvent: String, messageValue: MessageValue, force: Boolean = false)
}
