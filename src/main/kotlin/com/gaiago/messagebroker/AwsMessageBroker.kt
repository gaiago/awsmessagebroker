package com.gaiago.messagebroker

import com.gaiago.messagebroker.DynamoMessageRepository.Companion.defaultDynamoDb

class AwsMessageBroker(private val messageClient: MessageClient, private val messageRepository: MessageRepository) :
    MessageBroker {

    companion object {
        fun new(eventsTableName: String, topicPrefix: String = "") = AwsMessageBroker(
            SnsMessageClient(topicPrefix),
            DynamoMessageRepository(eventsTableName, defaultDynamoDb())
        )
    }

    override fun publish(messageEvent: String, messageValue: MessageValue, force: Boolean) {
        val message = Message(messageEvent, messageValue)
        if(notYetNotified(message)) {
            messageClient.publish(message)
            messageRepository.save(message)
        }
        else if(force) {
            messageClient.publish(message)
        }
    }

    private fun notYetNotified(message: Message) = !messageRepository.alreadyNotified(message)
}
