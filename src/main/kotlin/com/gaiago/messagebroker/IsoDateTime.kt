package com.gaiago.messagebroker

interface IsoDateTime {
    fun iso(): String
}