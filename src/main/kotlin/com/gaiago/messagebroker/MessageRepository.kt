package com.gaiago.messagebroker

interface MessageRepository {
    fun alreadyNotified(message: Message): Boolean
    fun save(message: Message)
}
