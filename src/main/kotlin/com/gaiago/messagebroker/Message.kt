package com.gaiago.messagebroker

import com.google.gson.GsonBuilder
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer

data class Message(val event: String, val value: MessageValue) {
    fun asString(): String {
        return GsonBuilder()
            .registerTypeHierarchyAdapter(IsoDateTime::class.java, JsonSerializer { dateTime: IsoDateTime, _, _ -> JsonPrimitive(dateTime.iso()) })
            .create()
            .toJson(mapOf(name() to value))
    }

    private fun name(): String {
        val name = value::class.java.simpleName
        return name.replaceFirst(name.first(), name.first().toLowerCase())
    }
}
