package com.gaiago.messagebroker

interface MessageClient {
    fun publish(message: Message)
}
