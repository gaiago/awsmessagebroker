package com.gaiago.messagebroker

import software.amazon.awssdk.regions.Region.EU_WEST_1
import software.amazon.awssdk.services.sns.SnsClient
import software.amazon.awssdk.services.sns.model.CreateTopicRequest
import software.amazon.awssdk.services.sns.model.PublishRequest

class SnsMessageClient(private val topicPrefix: String) : MessageClient {
    private val topics = hashMapOf<String, String>()
    private val snsClient = SnsClient.builder().region(EU_WEST_1).build()

    override fun publish(message: Message) {
        if (!topics.containsKey(message.event)) {
            val topic = CreateTopicRequest.builder().name(topicName(message)).build()
            val topicArn = snsClient.createTopic(topic).topicArn()
            topics[message.event] = topicArn
        }

        println("Publishing event: $message")
        snsClient.publish(build(message))
    }

    private fun build(message: Message) = PublishRequest.builder()
        .topicArn(topics[message.event])
        .message(message.asString())
        .subject(message.event)
        .build()

    private fun topicName(message: Message) = "${topicPrefix}_${message.event}"
}
