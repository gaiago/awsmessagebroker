package com.gaiago.messagebroker

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.testcontainers.containers.FixedHostPortGenericContainer
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
class DynamoMessageRepositoryTest {
    private var dynamoDb = DynamoDatabase.dynamoDb

    companion object {
        @Container
        val dynamodb = FixedHostPortGenericContainer<Nothing>("amazon/dynamodb-local").apply {
            withFixedExposedPort(8099, 8000)
            withReuse(true)
            waitingFor(LogMessageWaitStrategy().withRegEx(".*CorsParams.*").withTimes(1))
        }
    }

    @BeforeEach
    fun setUp() {
        DynamoDatabase.cleanup()
    }

    @Test
    fun `not yet notified`() {
        val message = Message("any", TestMessageValue(3))

        assertThat(DynamoMessageRepository("events", dynamoDb).alreadyNotified(message)).isFalse()
    }

    @Test
    fun `already notified`() {
        val message = Message("any", TestMessageValue(3))

        DynamoMessageRepository("events", dynamoDb).save(message)

        assertThat(DynamoMessageRepository("events", dynamoDb).alreadyNotified(message)).isTrue()
    }
}