package com.gaiago.messagebroker

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class AwsMessageBrokerTest {

    private val messageClient: MessageClient = mockk(relaxed = true)
    private val messageRepository: MessageRepository = mockk(relaxed = true)
    private val messageBroker = AwsMessageBroker(messageClient, messageRepository)

    @Test
    fun `publish event`() {
        val messageValue = TestMessageValue(1)
        val message = Message("any", messageValue)
        every { messageRepository.alreadyNotified(message) } returns false

        messageBroker.publish("any", messageValue)

        verify { messageClient.publish(message) }
        verify { messageRepository.save(message) }
    }

    @Test
    fun `do not publish event twice`() {
        val messageValue = TestMessageValue(1)
        every { messageRepository.alreadyNotified(Message("any", messageValue)) } returns true

        messageBroker.publish("any", messageValue)

        verify(exactly = 0) { messageClient.publish(any()) }
        verify(exactly = 0) { messageRepository.save(any()) }
    }

    @Test
    fun `save event with force`() {
        val messageValue = TestMessageValue(1)
        every { messageRepository.alreadyNotified(Message("any", messageValue)) } returns false

        messageBroker.publish("any", messageValue, force = true)

        verify(exactly = 1) { messageClient.publish(any()) }
        verify(exactly = 1) { messageRepository.save(any()) }
    }

    @Test
    fun `can publish event twice with force`() {
        val messageValue = TestMessageValue(1)
        every { messageRepository.alreadyNotified(Message("any", messageValue)) } returns true

        messageBroker.publish("any", messageValue, force = true)

        verify(exactly = 1) { messageClient.publish(any()) }
        verify(exactly = 0) { messageRepository.save(any()) }
    }
}