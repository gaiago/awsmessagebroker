package com.gaiago.messagebroker

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class MessageTest {

    @Test
    fun `key in camel case`() {
        val message = Message("any", TestMessageValue(3))

        assertThat(message.asString()).isEqualTo("""{"testMessageValue":{"id":3}}""")
    }
}