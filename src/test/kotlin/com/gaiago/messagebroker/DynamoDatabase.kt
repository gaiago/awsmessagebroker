package com.gaiago.messagebroker

import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.regions.Regions
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput

object DynamoDatabase {
    val dynamoDb = DynamoDB(
        AmazonDynamoDBClientBuilder.standard()
            .withEndpointConfiguration(
                AwsClientBuilder.EndpointConfiguration(
                    "http://localhost:8099",
                    Regions.EU_WEST_1.name
                )
            )
            .build()
    )

    fun cleanup() {
        deleteTable("events")
        createEventsTable()
    }

    private fun createEventsTable() {
        val keySchema = listOf(
            KeySchemaElement(
                "type",
                "HASH"
            ), KeySchemaElement("id", "RANGE")
        )
        val attributeDefinitions = listOf(
            AttributeDefinition(
                "type",
                "S"
            ), AttributeDefinition("id", "N")
        )
        val throughput = ProvisionedThroughput(5, 5)
        val request = CreateTableRequest(
            attributeDefinitions,
            "events",
            keySchema,
            throughput
        )

        val createTable = dynamoDb.createTable(request)
        createTable.waitForActive()
    }

    private fun deleteTable(name: String) {
        try {
            val table = dynamoDb.getTable(name)
            table.delete()
            table.waitForDelete()
        } catch (e: Exception) { }
    }
}